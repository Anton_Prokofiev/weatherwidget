
function testFunction(){
    document.querySelector('#button-primary').addEventListener('click', ()=>{
        console.log('btn was clicked')
        let city = document.getElementById('lname').value
        console.log(city)

        fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=859c1e138033b3b82cbaa26ca9364d61`)
        .then(function (response) {
            return response.json()
        })
        .then(function (data) {
            console.log(data);
            document.querySelector('.city-name').innerHTML = data.name;
            document.querySelector('.degrees').innerHTML = Math.round(data.main.temp - 273) +
                '&deg;';
            document.querySelector('.weather-description').innerHTML = data.weather[0]['description'];
            document.querySelector('.features li').innerHTML = `<img src = "http://openweathermap.org/img/wn/${data.weather[0]['icon']}@2x.png">`
        })
        .catch(() => {
    
        })
    }) 
}

testFunction()



